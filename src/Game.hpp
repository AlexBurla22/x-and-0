#ifndef GAME_H
#define GAME_H

#include "Tabla.hpp"
#include "Player.hpp"
#include "Settings.hpp"
#include <stdlib.h>
#include <time.h>
#include <list>
class Game{
    private:
        Player playerX;
        Player player0;
        Player currentPlayer;
        Tabla tablaJoc;
        Simbol randJoc;
        Settings settings;
        int longestSeries = 0;
        void CalculateOnLines();
        void CalculateOnColumns();
        void SetCurrentPlayer();
        bool CheckDraw();
        Simbol RandJocRandom();
        void CalculateDiagonalyLeftToRight();
        void CalculateDiagonalyRightToLeft();
    public:
        Game();
        Game(std::string numePlayerX, std::string numePlayer0, Settings settings);
        Tabla GetTabla();
        bool TryPlasareSimbol(int linie, int coloana, Player player);
        void SchimbareRand();
        Simbol GetRandJoc();
        void CalculateLongestSeries();
        Player GetCurrentPlayer();
        bool VerificaSfarsitJoc(std::string& message);
};
#endif