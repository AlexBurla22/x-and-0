#include "Player.hpp"

Player::Player(){
    this->nume = "";
    this->simbol = undefined;
}
Player::Player(std::string nume, Simbol simbol){
    this->nume = nume;
    this->simbol = simbol;
}
std::string Player::GetNume(){
    return this->nume;
}
void Player::SetNume(std::string nume){
    this->nume = nume;
}
Simbol Player::GetSimbol(){
    return this->simbol;
}
void Player::SetSimbol(Simbol simbol){
    this->simbol = simbol;
}