#ifndef DISPLAYINTERFACE_HPP
#define DISPLAYINTERFACE_HPP
#include "Tabla.hpp"
#include "Game.hpp"

class DisplayInterface{
    public:
        virtual void AfisareMeniu() = 0;
        virtual void AfisareTabla(Tabla tabla) = 0;
        virtual void AfisareRand(Game game) = 0;
        virtual void SfarsitJoc(std::string message) = 0;
};
#endif