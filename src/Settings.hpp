#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#define MAX_LINES 10
#define MIN_LINES 3
#define MAX_COLUMNS 10
#define MIN_COLUMNS 3

class Settings
{
private:
    int nrLinii;
    int nrColoane;
    int serieCastig;
public:
    Settings();
    Settings(int nrLinii, int nrColoane, int serieCastig);
    int GetNrLinii();
    void SetNrLinii(int nrLinii);
    int GetNrColoane();
    void SetNrColoane(int nrColoane);
    int GetSerieCastig();
    void SetSerieCastig(int serieCastig);
    ~Settings();
};
#endif