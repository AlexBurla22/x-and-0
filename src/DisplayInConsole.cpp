#include "DisplayInConsole.hpp"

void DisplayInConsole::AfisareMeniu(){
    system("cls");
    std::cout << "Please select an option: " << std::endl;
    std::cout << "1. Start Game" << std::endl;
    std::cout << "2. Settings" << std::endl;
    std::cout << "3. Exit" << std::endl;
}

void DisplayInConsole::ShowCurrentSettings(Settings settings){
    system("cls");
    
    std::cout << "Number of lines: " << settings.GetNrLinii() << "\n";
    std::cout << "Number of columns: " << settings.GetNrColoane() << "\n";
    std::cout << "Length of symbol series to win: " << settings.GetSerieCastig() << "\n";

    std::cout << std::endl;
}

void DisplayInConsole::AfisareTabla(Tabla tabla){
    std::cout << ' ';
    for (int nrc = 0; nrc < tabla.GetNrColoane(); nrc++)
    {
        std::cout << ' ' << nrc << ' ';
    }
    std::cout << std::endl;
    for (int linie = 0; linie < tabla.GetNrLinii(); linie++)
    {
        std::cout << linie;
        for (int coloana = 0; coloana < tabla.GetNrColoane(); coloana++)
        {
            std::cout<< '[';
            switch (tabla.GetPozitie(linie, coloana))
            {
                case simbolX:
                    std::cout << 'X';
                    break;
                case simbol0:
                    std::cout << '0';
                    break;
                case undefined:
                    std::cout << ' ';
                    break;
                default:
                    break;
            }
            std::cout << ']';
        }
        std::cout << std::endl;
    }
}

void DisplayInConsole::AfisareRand(Game game){
    std::cout << std::endl;
    char simbol;

    if (game.GetCurrentPlayer().GetSimbol() == simbolX)
    {
        simbol = 'X';
    }
    else if(game.GetCurrentPlayer().GetSimbol() == simbol0)
    {
        simbol = '0';
    }
    else
    {
        simbol = ' ';
    }
    std::cout << simbol << " player turn: " << game.GetCurrentPlayer().GetNume() << std::endl;
}

void DisplayInConsole::CitestePozitiaDorita(int& linia, int& coloana){
    std::cout << std::endl << "Please enter line: " << std::endl;

    std::string line;
    do
    {
        getline(std::cin, line);
    } while (!tryParse(line, linia));
    
    std::cout << "Please enter column: " << std::endl;

    std::string column;
    do
    {
        getline(std::cin, column);
    } while (!tryParse(column, coloana));
}

bool DisplayInConsole::ValidNumber(int value, int min, int max){
    if (value >= min && value < max)
    {
        return true;
    }
    std::cout << "Not a valid number" << std::endl;
    return false;
}
void DisplayInConsole::SfarsitJoc(std::string message){
    std::cout << std::endl;
    std::cout << "Game ended! " << message << std::endl;
    std::cout << "Press ENTER to return to menu...";
    std::cin.get();
}