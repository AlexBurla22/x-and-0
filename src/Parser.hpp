#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>
#include <iostream>

bool tryParse(std::string input, int& output);

#endif