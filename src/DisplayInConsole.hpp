#ifndef DISPLAYINCONSOLE_HPP
#define DISPLAYINCONSOLE_HPP
#include <iostream>
#include <string>
#include "Settings.hpp"
#include "Parser.hpp"
#include "DisplayInterface.hpp"

class DisplayInConsole: public DisplayInterface {
    public:
        void AfisareMeniu() override;
        void ShowCurrentSettings(Settings settings);
        void AfisareTabla(Tabla tabla) override;
        void AfisareRand(Game game) override;
        void CitestePozitiaDorita(int& linia, int& coloana);
        bool ValidNumber(int value, int min, int max);
        void SfarsitJoc(std::string message) override;
};
#endif