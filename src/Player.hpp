#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "Simbol.hpp"

class Player{
    private:
        std::string nume;
        Simbol simbol;
    public:
        Player();
        Player(std::string nume, Simbol simbol);
        std::string GetNume();
        void SetNume(std::string nume);
        Simbol GetSimbol();
        void SetSimbol(Simbol simbol);
};
#endif