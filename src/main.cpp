#include "Game.hpp"
#include "MenuItem.hpp"
#include <iostream>
#include <string>
#include "Parser.hpp"
#include "DisplayInConsole.hpp"

MenuItem GetSelectedItem()
{
    std::string option;
    int optionValue;

    do
    {
        getline(std::cin, option);
    } while (!tryParse(option, optionValue));

    switch (optionValue)
    {
    case 1:
        return START_GAME;
        break;
    case 2:
        return SETTINGS;
        break;
    case 3:
        return EXIT;
    default:
        return not_selected;
        break;
    }
}

void PlayGame(std::string playerXName, std::string player0Name, Settings settings)
{
    bool gata = false;
    Game newGame(playerXName, player0Name, settings);
    DisplayInConsole display;
    std::string outMessage;
    while (!gata)
    {
        display.AfisareRand(newGame);
        display.AfisareTabla(newGame.GetTabla());

        int linia;
        int coloana;

        display.CitestePozitiaDorita(linia, coloana);

        if (!newGame.TryPlasareSimbol(linia, coloana, newGame.GetCurrentPlayer()))
        {
            std::cout << "Unable, please choose another position!" << std::endl;
        }
        else
        {
            system("cls");
            newGame.CalculateLongestSeries();
            gata = newGame.VerificaSfarsitJoc(outMessage);

            if (!gata)
            {
                newGame.SchimbareRand();
            }
        }
    }
    display.SfarsitJoc(outMessage);
}
int GetLinesFromConsole()
{
    std::string lines;
    int lineInt;
    bool validInput = false;

    while (!validInput)
    {
        do
        {
            getline(std::cin, lines);
        } while (!tryParse(lines, lineInt));
        if (lineInt >= MIN_LINES && lineInt <= MAX_LINES)
        {
            validInput = true;
        }
        else
        {
            std::cout << "Please enter a value from " << MIN_LINES << " to " << MAX_LINES << std::endl;
        }
    }
    return lineInt;
}
int GetColsFromConsole()
{
    std::string cols;
    int colInt;
    bool validInput = false;

    while (!validInput)
    {
        do
        {
            getline(std::cin, cols);
        } while (!tryParse(cols, colInt));
        if (colInt >= MIN_COLUMNS && colInt <= MAX_COLUMNS)
        {
            validInput = true;
        }
        else
        {
            std::cout << "Please enter a value from " << MIN_COLUMNS << " to " << MAX_COLUMNS << std::endl;
        }
    }
    return colInt;
}
int GetSeriesFromConsole(int currentNoLines)
{
    std::string series;
    int serInt;
    bool validInput = false;

    while (!validInput)
    {
        do
        {
            getline(std::cin, series);
        } while (!tryParse(series, serInt));
        if (serInt >= MIN_LINES && serInt <= currentNoLines)
        {
            validInput = true;
        }
        else
        {
            std::cout << "Series number must be between "<< MIN_LINES << " and " << currentNoLines << std::endl;
        }
    }
    return serInt;
}
void ChangeSettings(Settings &settings)
{
    std::cout << "Enter new number of lines: ";
    settings.SetNrLinii(GetLinesFromConsole());

    std::cout << "Enter new number of columns: ";
    settings.SetNrColoane(GetColsFromConsole());

    std::cout << "Enter new number of simbol series to win: ";
    settings.SetSerieCastig(GetSeriesFromConsole(settings.GetNrLinii()));
}
int main()
{
    Settings gameSettings;
    bool mustExit = false;
    DisplayInConsole display;
    while (!mustExit)
    {
        display.AfisareMeniu();

        MenuItem selected = GetSelectedItem();
        while (selected == not_selected)
        {
            std::cout << "Not a valid option!" << std::endl;
            selected = GetSelectedItem();
        }

        switch (selected)
        {
        case START_GAME:
        {
            system("cls");
            std::cout << "New Game started\n\n";

            std::string playerXName;
            std::string player0Name;

            std::cout << "Please enter player X name: ";
            getline(std::cin, playerXName);

            std::cout << "Please enter player 0 name: ";
            getline(std::cin, player0Name);

            PlayGame(playerXName, player0Name, gameSettings);
            break;
        }
        case SETTINGS:
        {
            system("cls");
            std::cout << "Change Settings\n\n";
            display.ShowCurrentSettings(gameSettings);
            ChangeSettings(gameSettings);
            std::cout << "Press ENTER to return to menu...";
            std::cin.get();
            break;
        }
        case EXIT:
        {
            mustExit = true;
            break;
        }
        default:
        {
            mustExit = true;
            break;
        }
        }
    }
    return 0;
}