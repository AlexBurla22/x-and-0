#include "Tabla.hpp"

Tabla::Tabla(){
    nrLinii = 0;
    nrColoane = 0;
    InitializareTabla();
}

Tabla::Tabla(int nrLinii, int nrColoane){
    this->nrLinii = nrLinii;
    this->nrColoane = nrColoane;
    
    this->patratele.resize(nrLinii);

    for (int index = 0; index < nrLinii; index++)
    {
        patratele[index].resize(nrColoane);
    }
    InitializareTabla();
}

void Tabla::InitializareTabla(){
    for (int linie = 0; linie < nrLinii; linie++)
    {
        for (int coloana = 0; coloana < nrColoane; coloana++)
        {
            patratele[linie][coloana] = undefined;
        }
    }
}

std::vector<std::vector<Simbol>> Tabla::GetPatratele(){
    return patratele;
}

Simbol Tabla::GetPozitie(int linie, int coloana){
    return patratele[linie][coloana];
}

void Tabla::SetPozitie(int linie, int coloana, Simbol simbol){
    this->patratele[linie][coloana] = simbol;
}

int Tabla::GetNrLinii(){
    return this->nrLinii;
}

int Tabla::GetNrColoane(){
    return this->nrColoane;
}