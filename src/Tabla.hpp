#ifndef TABLA_HPP
#define TABLA_HPP
#include "Simbol.hpp"
#include <vector>

class Tabla{
    private:
        int nrLinii;
        int nrColoane;
        std::vector<std::vector<Simbol>> patratele;
        void InitializareTabla();
    public:
        Tabla();
        Tabla(int nrLinii, int nrColoane);
        int GetNrLinii();
        int GetNrColoane();
        std::vector<std::vector<Simbol>> GetPatratele();
        void SetPozitie(int linie, int coloana, Simbol simbol);
        Simbol GetPozitie(int linie, int coloana);
};
#endif