#include "Game.hpp"

Game::Game()
{
    this->tablaJoc = Tabla(settings.GetNrLinii(), settings.GetNrColoane());
    this->playerX = Player("PlayerX", simbolX);
    this->player0 = Player("Player0", simbol0);
    this->randJoc = RandJocRandom();
    SetCurrentPlayer();
}

Game::Game(std::string numePlayerX, std::string numePlayer0, Settings settings)
{
    this->settings = settings;
    this->tablaJoc = Tabla(settings.GetNrLinii(), settings.GetNrColoane());
    this->playerX = Player(numePlayerX, simbolX);
    this->player0 = Player(numePlayer0, simbol0);
    this->randJoc = RandJocRandom();
    SetCurrentPlayer();
}

void Game::SetCurrentPlayer()
{
    if (randJoc == simbolX)
    {
        currentPlayer = playerX;
    }
    else if (randJoc == simbol0)
    {
        currentPlayer = player0;
    }
}

Player Game::GetCurrentPlayer()
{
    return currentPlayer;
}

Tabla Game::GetTabla()
{
    return this->tablaJoc;
}

Simbol Game::RandJocRandom()
{
    srand(time(NULL));
    if (rand() % 2)
    {
        return simbolX;
    }
    return simbol0;
}

void Game::SchimbareRand()
{
    if (randJoc == simbolX)
    {
        randJoc = simbol0;
    }
    else if (randJoc == simbol0)
    {
        randJoc = simbolX;
    }
    SetCurrentPlayer();
}

Simbol Game::GetRandJoc()
{
    return this->randJoc;
}
bool Game::VerificaSfarsitJoc(std::string &message)
{
    if (longestSeries == settings.GetSerieCastig())
    {
        if (randJoc == simbolX)
        {
            message = "Winner is " + playerX.GetNume();
        }
        else if (randJoc == simbol0)
        {
            message = "Winner is " + player0.GetNume();
        }
        return true;
    }
    else if (CheckDraw())
    {
        message = "Game is a draw!";
        return true;
    }
    return false;
}
bool Game::CheckDraw()
{
    for (int line = 0; line < tablaJoc.GetNrLinii(); line++)
    {
        for (int col = 0; col < tablaJoc.GetNrColoane(); col++)
        {
            if (tablaJoc.GetPozitie(line, col) == undefined)
            {
                return false;
            }
        }
    }
    return true;
}

void Game::CalculateDiagonalyLeftToRight()
{
    for (int i = 0; i < tablaJoc.GetNrLinii(); i++)
    {
        int row = i;
        int col = 0;

        std::vector<Simbol> diagonala;
        while (row >= 0 && col < tablaJoc.GetNrColoane())
        {
            diagonala.push_back(tablaJoc.GetPozitie(row, col));
            col++;
            row--;
        }
        int temp = 1;
        for (int ind = 0; ind < diagonala.size() - 1; ind++)
        {
            if (diagonala[ind] == diagonala[ind + 1] && diagonala[ind] == randJoc)
            {
                temp++;
            }
            else
            {
                if (temp > longestSeries)
                {
                    longestSeries = temp;
                }
                temp = 1;
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
    for (int j = 1; j < tablaJoc.GetNrColoane(); j++)
    {
        int row = tablaJoc.GetNrLinii() - 1;
        int col = j;
        std::vector<Simbol> diagonala;

        while (row >= 0 && col < tablaJoc.GetNrColoane())
        {
            diagonala.push_back(tablaJoc.GetPozitie(row, col));
            col++;
            row--;
        }
        int temp = 1;
        for (int ind2 = 0; ind2 < diagonala.size() - 1; ind2++)
        {
            if (diagonala[ind2] == diagonala[ind2 + 1] && diagonala[ind2] == randJoc)
            {
                temp++;
            }
            else
            {
                if (temp > longestSeries)
                {
                    longestSeries = temp;
                }
                temp = 1;
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
}
void Game::CalculateDiagonalyRightToLeft()
{
    for (int i = 0; i < tablaJoc.GetNrLinii(); i++)
    {
        int row = i;
        int col = tablaJoc.GetNrColoane() - 1;

        std::vector<Simbol> diagonala;
        while (row >= 0 && col >= 0)
        {
            diagonala.push_back(tablaJoc.GetPozitie(row, col));
            col--;
            row--;
        }
        int temp = 1;
        for (int ind = 0; ind < diagonala.size() - 1; ind++)
        {
            if (diagonala[ind] == diagonala[ind + 1] && diagonala[i] == randJoc)
            {
                temp++;
            }
            else
            {
                if (temp > longestSeries)
                {
                    longestSeries = temp;
                }
                temp = 1;
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
    for (int j = 1; j < tablaJoc.GetNrColoane(); j++)
    {
        int row = tablaJoc.GetNrLinii() - 1;
        int col = j;
        std::vector<Simbol> diagonala;

        while (row >= 0 && col >=0)
        {
            diagonala.push_back(tablaJoc.GetPozitie(row, col));
            col--;
            row--;
        }
        int temp = 1;
        for (int ind2 = 0; ind2 < diagonala.size() - 1; ind2++)
        {
            if (diagonala[ind2] == diagonala[ind2 + 1] && diagonala[ind2] == randJoc)
            {
                temp++;
            }
            else
            {
                if (temp > longestSeries)
                {
                    longestSeries = temp;
                }
                temp = 1;
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
}
bool Game::TryPlasareSimbol(int linie, int coloana, Player player)
{
    if (linie >= 0 && linie < tablaJoc.GetNrLinii())
    {
        if (coloana >= 0 && coloana < tablaJoc.GetNrColoane())
        {
            if (randJoc == player.GetSimbol())
            {
                if (tablaJoc.GetPozitie(linie, coloana) == undefined)
                {
                    tablaJoc.SetPozitie(linie, coloana, player.GetSimbol());
                    return true;
                }
            }
        }
    }
    return false;
}

void Game::CalculateOnLines()
{
    for (int linie = 0; linie < tablaJoc.GetNrLinii(); linie++)
    {
        int temp = 1;
        for (int col = 0; col < tablaJoc.GetNrColoane() - 1; col++)
        {
            if (tablaJoc.GetPozitie(linie, col) == randJoc)
            {
                if (tablaJoc.GetPozitie(linie, col) == tablaJoc.GetPozitie(linie, col + 1))
                {
                    temp++;
                }
                else
                {
                    if (temp > longestSeries)
                    {
                        longestSeries = temp;
                    }
                    temp = 1;
                }
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
}
void Game::CalculateOnColumns()
{
    for (int col = 0; col < tablaJoc.GetNrColoane(); col++)
    {
        int temp = 1;
        for (int linie = 0; linie < tablaJoc.GetNrLinii() - 1; linie++)
        {
            if (tablaJoc.GetPozitie(linie, col) == randJoc)
            {
                if (tablaJoc.GetPozitie(linie, col) == tablaJoc.GetPozitie(linie + 1, col))
                {
                    temp++;
                }
                else
                {
                    if (temp > longestSeries)
                    {
                        longestSeries = temp;
                    }
                    temp = 1;
                }
            }
        }
        if (temp > longestSeries)
        {
            longestSeries = temp;
        }
    }
}

void Game::CalculateLongestSeries()
{
    CalculateOnLines();
    CalculateOnColumns();
    CalculateDiagonalyLeftToRight();
    CalculateDiagonalyRightToLeft();
}