#include "Settings.hpp"
Settings::Settings()
{
    nrLinii = MIN_LINES;
    nrColoane = MIN_COLUMNS;
    serieCastig = MIN_LINES;
}
Settings::Settings(int nrLinii, int nrColoane, int serieCastig){
    this->nrLinii = nrLinii;
    this->nrColoane = nrColoane;
    this->serieCastig = serieCastig;
}
int Settings::GetNrLinii(){
    return nrLinii;
}
void Settings::SetNrLinii(int nrLinii){
    this->nrLinii = nrLinii;
}
int Settings::GetNrColoane(){
    return nrColoane;
}
void Settings::SetNrColoane(int nrColoane){
    this->nrColoane = nrColoane;
}
int Settings::GetSerieCastig(){
    return serieCastig;
}
void Settings::SetSerieCastig(int serieCastig){
    this->serieCastig = serieCastig;
}
Settings::~Settings()
{
}