#include "Parser.hpp"

bool tryParse(std::string input, int& output){
    try
    {
        output = std::stoi(input);
    }
    catch(std::invalid_argument)
    {
        std::cout << "Not a number\n";
        return false;
    }
    return true;
}